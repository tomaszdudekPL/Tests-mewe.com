const parentConfig = require('./wdio.conf').config

exports.config = Object.assign({}, parentConfig, {
  baseUrl: 'https://mewe.com',
})