const parentConfig = require('./wdio.conf').config;

exports.config = Object.assign({}, parentConfig, {
    baseUrl: 'http://localhost:8001/'
});
