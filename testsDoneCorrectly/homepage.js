describe('Homepage is accessible', () => {
  it('should be possible for any user to log in', () => {
    browser
      .url('/')
      .getTitle().should.be.equal('MeWe - The Next-Gen Social Network')
  })
})